set(libtorch_dir "${external_dir}/libtorch")

list(APPEND CMAKE_PREFIX_PATH "${libtorch_dir}")
find_package(Torch REQUIRED)

add_library(TorchWrapper INTERFACE)
target_include_directories(TorchWrapper INTERFACE "${libtorch_dir}/include")
target_link_libraries(TorchWrapper
    INTERFACE "${TORCH_LIBRARIES}"
)

