#include <torch/torch.h>
#include <limits>
#include <iostream>

int main(int argc, char **argv) {
    std::cout << "Tensors\n";

    std::cout << "\nAn Empty 5x3 Tensor\n";
    auto x = torch::empty({5,3});
    std::cout << x << '\n';

    std::cout << "\nA Randomly Initialised Matrix\n";
    x = torch::rand({5,3});
    std::cout << x << '\n';

    std::cout << "\nA Matrix Filled with Zeroes\n";
    x = torch::zeros({5,3}, torch::kLong);
    std::cout << x << '\n';

    std::cout << "\nA Sequence\n";
    x = torch::linspace(0, 9, 10, torch::kDouble);
    std::cout << x << '\n';

    std::cout << "\nCreate Tensors from Existing Tensor\n";
    auto y = x.new_full({5,3}, 1, torch::kDouble);
    std::cout << y << '\n';
    auto z = torch::randn_like(y, torch::kFloat);
    std::cout << z << '\n';

    std::cout << "\nGet the Size\n";
    std::cout << z.sizes() << '\n';


    std::cout << "\n\nOperations\n";

    std::cout << "\nAddition 'x + y'\n";
    x = torch::rand({5,3});
    y = torch::rand({5,3});
    std::cout << x + y << '\n';

    std::cout << "\nAddition 'torch::add(x,y)'\n";
    std::cout << torch::add(x,y) << '\n';

    std::cout << "\nAddition 'torch::add_out(result, x, y)'\n";
    auto result = torch::empty({5,3});
    torch::add_out(result, x, y);
    std::cout << result << '\n';

    std::cout << "\nAddition in place 'y.add_(x)'\n";
    y.add_(x);
    std::cout << y << '\n';

    std::cout << "\nSum of all elements 'x.sum()'\n";
    std::cout << x.sum() << '\n';

    std::cout << "\nIndexing\n";
    std::cout << x << '\n';
    std::cout << x[1] << '\n';
    std::cout << x.t()[1] << '\n';
    std::cout << x[1][2] << '\n';
    std::cout << x.index({1,2}) << '\n';
    std::cout << x.index({torch::indexing::Slice(0,3), torch::indexing::Slice(0,2)}) << '\n';

    std::cout << "\nViews\n";
    x = torch::randn({4,4});
    y = x.view(16);
    z = x.view({-1, 8});
    std::cout << x << '\n';
    std::cout << y << '\n';
    std::cout << z << '\n';

    std::cout << "\nAccess Contents of One Element Tensor\n";
    x = torch::randn({1});
    std::cout << x << '\n';
    std::cout << x.item<float>() << '\n';

    std::cout << "\nCheck for NaNs\n";
    x = torch::randn({10});
    x[4] = std::numeric_limits<double>::quiet_NaN();
    std::cout << "x.isnan() =\n";
    std::cout << x.isnan() << '\n';

    return EXIT_SUCCESS;
}

