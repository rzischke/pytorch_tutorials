#define _USE_MATH_DEFINES
#include <torch/torch.h>
#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>
#include <memory>
#include <stdexcept>
#include <vector>

using namespace torch::indexing;

class ProbabilisticModule : public torch::nn::Module {
    public:

        virtual torch::Tensor forward(torch::Tensor x) const {
            throw std::runtime_error("ProbabilisticModuel::forward unimplemented.");
        }

        virtual int minimum_past_size(void) const {
            throw std::runtime_error("ProbabilisticModule::minimum_past_size unimplemented.");
        }

        virtual torch::Tensor probability_density_function(const torch::Tensor& forecasts, const torch::Tensor& observations) const {
            throw std::runtime_error("ProbabilisticModule::probability_density_function unimplemented.");
        }

        virtual torch::Tensor log_probability_density_function(const torch::Tensor& forecasts, const torch::Tensor& observations) const {
            return probability_density_function(forecasts, observations).log();
        }

        virtual torch::Tensor probability_mass_function(const torch::Tensor& forecasts, const torch::Tensor& observations) const {
            throw std::runtime_error("ProbabilisticModule::probability_mass_function unimplemented.");
        }

        virtual torch::Tensor log_probability_mass_function(const torch::Tensor& forecasts, const torch::Tensor& observations) const {
            return probability_mass_function(forecasts, observations).log();
        }

        virtual torch::Tensor cumulative_distribution_function(const torch::Tensor& forecasts, const torch::Tensor& observations) const {
            throw std::runtime_error("ProbabilisticModule::cumulative_distribution_function unimplemented.");
        }

        virtual torch::Tensor draw(long int sample_size, long int burn_in_size, double first_draw) const {
            throw std::runtime_error("ProbabilisticModule::draw unimplemented.");
        }

        virtual ~ProbabilisticModule() { }
};

torch::Tensor average_log_score(
    const ProbabilisticModule& module,
    const torch::Tensor& forecasts,
    const torch::Tensor& observations
) {
    return module.log_probability_density_function(forecasts, observations).mean();
}

struct Ar1 : ProbabilisticModule {
    Ar1(double alpha_in, double sigma2_in) {
        assert(sigma2_in > 0.0);
        alpha = register_parameter("alpha", torch::full({1}, alpha_in, torch::kDouble));
        sigma2 = register_parameter("sigma2", torch::full({1}, sigma2_in, torch::kDouble));
    }

    torch::Tensor forward(torch::Tensor x) const override {
        auto sizes = x.sizes();
        assert(sizes.size() == 1);
        assert(sizes[0] >= 1);

        // Forecast distribution is N(u,v).
        // output array with time index on rows and [u,v] on cols.
        x = x.reshape({sizes[0],1});
        x = alpha*x;
        auto sigma2_vec = x.new_full({sizes[0],1}, 1.0, torch::kDouble) * sigma2;
        x = torch::cat({x, sigma2_vec}, 1);

        return x;
    }

    virtual torch::Tensor log_probability_density_function(const torch::Tensor& forecasts, const torch::Tensor& observations) const override {
        constexpr double log_2pi = 1.837877066409345483560659472811235279722794947275566825634;

        auto forecasts_sizes = forecasts.sizes();
        assert(forecasts_sizes.size() == 2);
        assert(forecasts_sizes[1] == 2);

        auto observations_sizes = observations.sizes();
        assert(observations_sizes.size() == 1);
        assert(observations_sizes[0] == forecasts_sizes[0]);

        auto means = forecasts.index({Slice(None, None), mu_idx});
        auto variances = forecasts.index({Slice(None, None), sigma2_idx});

        auto observations_minus_means = observations - means;
        auto studentised_observations_squared = (observations_minus_means * observations_minus_means)/variances;
        return -0.5*(log_2pi + variances.log() + studentised_observations_squared);
    }

    torch::Tensor draw(long int sample_size, long int burn_in_size, double first_draw) const override { // first_draw needs to be a tensor.
        auto total_size = sample_size + burn_in_size;
        auto alpha_d = alpha.item<double>();
        auto sigma2_d = sigma2.item<double>();

        auto noise = torch::normal(0.0, sigma2_d, {total_size}, c10::nullopt, torch::kDouble);
        //auto noise = torch::normal(0.0, sigma2_d, {total_size}, nullptr, torch::kDouble);
        auto noise_a = noise.accessor<double,1>();

        auto sample = torch::empty({sample_size}, torch::kDouble);
        auto sample_a = sample.accessor<double,1>();

        auto burn_in_a = std::vector<double>(burn_in_size);

        burn_in_a[0] = first_draw;
        for (auto i = static_cast<decltype(burn_in_size)>(1); i < burn_in_size; ++i) {
            burn_in_a[i] = alpha_d*burn_in_a[i-1] + noise_a[i];
        }

        sample_a[0] = alpha_d*burn_in_a.back() + noise_a[burn_in_size];
        for (auto i = static_cast<decltype(sample_size)>(1); i < sample_size; ++i) {
            sample_a[i] = alpha_d*sample_a[i-1] + noise_a[burn_in_size + i];
        }

        return sample;
    }

    torch::Tensor alpha, sigma2;
    constexpr static int mu_idx = 0;
    constexpr static int sigma2_idx = 1;
};    

int main(int argc, char **argv) {
    std::cout << "ar1.cpp\n";

    double alpha = 0.5;
    std::cout << "\nalpha = " << alpha << '\n';
    
    double sigma2 = 1.0;
    std::cout << "sigma2 = " << sigma2 << '\n';

    std::unique_ptr<ProbabilisticModule> ar1_dgp = std::make_unique<Ar1>(alpha, sigma2); 

    std::cout << "\nar1_dgp_test_draws =\n";
    auto ar1_dgp_test_draws = ar1_dgp->draw(10, 100, 0.0);
    std::cout << ar1_dgp_test_draws << '\n';

    std::cout << "\nar1_dgp_test_draws.index(Slice(5,10))\n";
    std::cout << ar1_dgp_test_draws.index({Slice(5,10)}) << '\n';

    auto sample = ar1_dgp->draw(10000, 10000, 0.0);
    auto past = sample.index({Slice(None, sample.sizes()[0]-1)});
    auto future = sample.index({Slice(1, None)});
    auto best_forecasts = ar1_dgp->forward(past);
    std::cout << "best_forecasts[0:10] =\n";
    std::cout << best_forecasts.index({Slice(0,10)}) << '\n';

    std::cout << "past.sizes() =\n";
    std::cout << past.sizes() << '\n';

    std::cout << "future.sizes() =\n";
    std::cout << future.sizes() << '\n';

    std::unique_ptr<ProbabilisticModule> ar1_model = std::make_unique<Ar1>(0.2, 2.0);

    torch::optim::LBFGSOptions optimiser_options;   // Use default options.
    torch::optim::LBFGS optimiser(ar1_model->parameters(), optimiser_options);

    auto optimiser_closure = [&past, &future, &ar1_model, &optimiser] () {
        optimiser.zero_grad();
        auto forecasts = ar1_model->forward(past);
        auto loss = -average_log_score(*ar1_model, forecasts, future);
        loss.backward();
        return loss;
    };

    size_t max_iter = 10000;
    auto loss_after_step_prev = optimiser.step(optimiser_closure);
    auto loss_after_step = optimiser.step(optimiser_closure);
    while (!torch::equal(loss_after_step, loss_after_step_prev) && --max_iter) {
        loss_after_step_prev = loss_after_step;
        auto loss_after_step = optimiser.step(optimiser_closure);
    }

    std::cout << "max_iter = " << max_iter << '\n';

    std::cout << "trained params\n";
    for (const auto& pair: ar1_model->named_parameters()) {
        std::cout << pair.key() << ":\n" << pair.value() << '\n';
    }
    
    return EXIT_SUCCESS;
}

