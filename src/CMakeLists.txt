cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

add_executable(hello_pytorch "hello_pytorch.cpp")
target_link_libraries(hello_pytorch TorchWrapper)

add_executable(getting_started "getting_started.cpp")
target_link_libraries(getting_started TorchWrapper)

add_executable(autograd "autograd.cpp")
target_link_libraries(autograd TorchWrapper)

add_executable(ar1 "ar1.cpp")
target_link_libraries(ar1 TorchWrapper)

