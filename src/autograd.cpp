#include <algorithm>
#include <torch/torch.h>
#include <exception>
#include <iostream>
#include <vector>
#include <functional>

template<class T>
struct std::hash<std::vector<T>> {
    std::size_t operator()(const std::vector<T>& v) {
        std::hash<T> hash_T;
        std::size_t out = 0;
        for (const auto& t : v) {
            out ^= hash_T(t);
        }
        return out;
    }
};

int main(int argc, char **argv) {
    std::cout << "autograd\n";

    std::cout << "\nx =\n";
    auto x = torch::ones({2,2}, torch::requires_grad());
    std::cout << x << '\n';

    std::cout << "\ny = x + 2 =\n";
    auto y = x + 2;
    y = torch::add(x, 2);
    std::cout << y << '\n';

    std::cout << "\ny.grad_fn()->name() =\n";
    std::cout << y.grad_fn()->name() << '\n';

    std::cout << "\nz = y * y * 3 =\n";
    auto z = y * y * 3;
    std::cout << z << '\n';

    std::cout << "\nout = z.mean() =\n";
    auto out = z.mean();
    std::cout << out << '\n';

    std::cout << "\nRequires Grad In Place\n";
    auto a = torch::randn({2,2});
    a = ((a * 3) / (a - 1));
    std::cout << a.requires_grad() << '\n';
    a.requires_grad_(true);
    std::cout << a.requires_grad() << '\n';
    auto b = (a * a).sum();
    std::cout << b.grad_fn()->name() << '\n';

    std::cout << "\nGradients\n";
    out.backward();
    std::cout << "x.grad() =\n";
    std::cout << x.grad() << '\n';

    std::cout << "\nvector-Jacobian product\n";

    std::cout << "x =\n";
    x = torch::randn({3}, torch::requires_grad());
    std::cout << x << '\n';

    std::cout << "y =\n";
    y = x * 2;
    while (y.norm().item<double>() < 1000) { y *= 2; }
    std::cout << y << '\n';

    std::cout << "v =\n";
    float vdata[] = {0.1, 1.0, 0.0001};
    auto v = torch::from_blob(vdata, {1,3}, torch::kFloat);
    std::cout << v << '\n';

    std::cout << "(dy/dx) v^T =\n";
    y.backward(v);
    std::cout << x.grad() << '\n';

    std::cout << "\nDetatch when Gradient is No Longer Needed\n";
    std::cout << x.requires_grad() << '\n';
    y = x.detach();
    std::cout << y.requires_grad() << '\n';
    std::cout << x.eq(y).all().item<bool>() << '\n';

    std::cout << "\nDoes the gradient graph persist after setting elements individually?\n";
    auto p = torch::full({1}, 1.0, torch::requires_grad().dtype(torch::kDouble));
    auto q = torch::full({1}, 2.0, torch::requires_grad().dtype(torch::kDouble));
    auto r = torch::empty({2}, torch::kDouble);
    r.index_put_({0}, p);
    r.index_put_({1}, p*q);
    auto s = r.sum(torch::kDouble);
    try {
        s.backward();
        std::cout << "Yes!\n";
        std::cout << "p.grad() =\n";
        std::cout << p.grad() << '\n';
        std::cout << "q.grad() =\n";
        std::cout << q.grad() << '\n'; 
    } catch (std::exception& e) {
        std::cout << "No. An exception was thrown:\n";
        std::cout << "  what(): " << e.what() << '\n';
    }

    /*
    std::cout << "\nautograd::grad\n";
    std::cout << "A =\n";
    double A_blob[] = {
        2.0, 3.0,
        0.5, 1.0
    };
    auto A = torch::from_blob(A_blob, {2,2}, torch::kDouble);
    std::cout << A << '\n';
    std::cout << "x =\n";
    double w_blob[] = {1.0, 2.0, 3.0, 4.0};
    auto w = torch::from_blob(w_blob, {4,1}, torch::requires_grad().dtype(torch::kDouble));
    x = w.index({torch::indexing::Slice(0,2), torch::indexing::Ellipsis});
    std::cout << x << '\n';
    std::cout << "y =\n";
    y = w.index({torch::indexing::Slice(2,4), torch::indexing::Ellipsis});
    std::cout << y << '\n';
    std::cout << "z =\n";
    z = torch::chain_matmul({(x+y).transpose(0,1), A, 2*x-y});
    std::cout << z << '\n';
    std::cout << "z_grad =\n";
    auto w2 = w;
    std::vector<int64_t> z_grad_sizes;
    z_grad_sizes.insert(z_grad_sizes.end(), z.sizes().begin(), z.sizes().end());
    z_grad_sizes.insert(z_grad_sizes.end(), w.sizes().begin(), w.sizes().end());
    auto z_grad = torch::autograd::grad({z}, {w2}, {}, true, true).at(0).reshape(z_grad_sizes).squeeze();
    std::cout << z_grad << '\n';
    std::cout << "2(A+A')x + (2A'-A)y =\n";
    auto At = A.transpose(0,1);
    auto ApAt = A + A.transpose(0,1);
    auto TwoAtmA = 2.0*At - A;
    auto TwoAmAt = TwoAtmA.transpose(0,1);
    std::cout << torch::matmul(2.0*ApAt, x) + torch::matmul(TwoAtmA, y) << '\n';
    std::cout << "-(A+A')y + (2A-A')x=\n";
    std::cout << -torch::matmul(ApAt, y) + torch::matmul(TwoAmAt, x) << '\n';
    auto grad_output = torch::zeros_like(z_grad);
    std::vector<torch::Tensor> z_hess_vec;
    for (int64_t i = 0; i != z_grad.numel(); ++i) {
        grad_output.index_put_({i}, 1.0);
        std::cout << "z_hess_vec_" << i << " =\n";
        auto z_hess_vec_i = torch::autograd::grad({z_grad}, {w}, {grad_output}, true).at(0);
        std::cout << z_hess_vec_i << '\n';
        z_hess_vec.emplace_back(std::move(z_hess_vec_i));
        grad_output.index_put_({i}, 0.0);
    }
    std::cout << "z_hess =\n";
    std::vector<int64_t> z_hess_sizes;
    z_hess_sizes.insert(z_hess_sizes.end(), z_grad.sizes().begin(), z_grad.sizes().end());
    z_hess_sizes.insert(z_hess_sizes.end(), w.sizes().begin(), w.sizes().end());
    auto z_hess = torch::stack(z_hess_vec).reshape(z_hess_sizes).squeeze();
    std::cout << z_hess << '\n';
    std::cout << "A+A' =\n";
    std::cout << ApAt << '\n';
    std::cout << "2A-A' =\n";
    std::cout << TwoAmAt << '\n';
    std::cout << "2A'-A =\n";
    std::cout << TwoAtmA << '\n';
    */

    std::cout << "\nautograd::grad\n";
    std::cout << "A =\n";
    double A_blob[] = {
        2.0, 3.0,
        0.5, 1.0
    };
    auto A = torch::from_blob(A_blob, {2,2}, torch::kDouble);
    std::cout << A << '\n';
    std::cout << "x =\n";
    double w_blob[] = {1.0, 2.0, 3.0, 4.0};
    auto w = torch::from_blob(w_blob, {4,1}, torch::requires_grad().dtype(torch::kDouble));
    x = w.index({torch::indexing::Slice(0,2), torch::indexing::Ellipsis});
    auto xt = x.transpose(0,1);
    std::cout << x << '\n';
    std::cout << "y =\n";
    y = w.index({torch::indexing::Slice(2,4), torch::indexing::Ellipsis});
    auto yt = y.transpose(0,1);
    std::cout << y << '\n';
    std::cout << "z =\n";
    auto z1 = torch::chain_matmul({(x+y).transpose(0,1), A, 2*x-y});
    auto z2 = torch::chain_matmul({(2*x+y).transpose(0,1), A, x-y});
    z = torch::cat({z1.reshape({1}), z2.reshape({1})});
    std::cout << z << '\n';
    auto grad_output = torch::zeros_like(z);
    std::vector<torch::Tensor> z_grad_vec;
    for (int64_t i = 0; i != z.numel(); ++i) {
        grad_output.index_put_({i}, 1.0);
        std::cout << "z_grad_vec_" << i << " =\n";
        auto z_grad_vec_i = torch::autograd::grad({z}, {w}, {grad_output}, true).at(0);
        std::cout << z_grad_vec_i << '\n';
        z_grad_vec.emplace_back(z_grad_vec_i);
        grad_output.index_put_({i}, 0.0);
    }
    std::vector<int64_t> z_grad_shape;
    z_grad_shape.insert(z_grad_shape.end(), z.sizes().cbegin(), z.sizes().cend());
    z_grad_shape.insert(z_grad_shape.end(), w.sizes().cbegin(), w.sizes().cend());
    auto z_grad_stack = torch::stack(z_grad_vec);
    std::cout << "z_grad_stack =\n";
    std::cout << z_grad_stack.squeeze() << '\n';
    auto z_grad = torch::stack(z_grad_vec).reshape(z_grad_shape).squeeze();
    std::cout << "z_grad =\n";
    std::cout << z_grad << '\n';
    auto At = A.transpose(0,1);
    auto ApAt = A + At;
    auto TwoAmAt = 2.0*A - At;
    auto TwoAtmA = 2.0*At - A;
    auto AmTwoAt = A - 2.0*At;
    auto AtmTwoA = At - 2.0*A;
    auto dz1dx = 2.0*torch::matmul(xt, ApAt) + torch::matmul(yt, TwoAmAt);
    auto dz1dy = -torch::matmul(yt, ApAt) + torch::matmul(xt, TwoAtmA);
    auto dz2dx = 2.0*torch::matmul(xt, ApAt) + torch::matmul(yt, AmTwoAt);
    auto dz2dy = -torch::matmul(yt, ApAt) + torch::matmul(xt, AtmTwoA);
    std::cout << "dz1dx =\n" << dz1dx << '\n';
    std::cout << "dz1dy =\n" << dz1dy << '\n';
    std::cout << "dz2dx =\n" << dz2dx << '\n';
    std::cout << "dz2dy =\n" << dz2dy << '\n';

    return EXIT_SUCCESS;
}

